package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows;
    private int cols;
    private CellState[][] cellGrid;


    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        cellGrid = new CellState[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                cellGrid[i][j] = initialState;
            }
        }
    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    private boolean outOfBounds(int row, int column) {
        if (row >= 0 && row < numRows() || column >= 0 && column < numColumns()) {
            return false;
        }
        return true;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (outOfBounds(row, column))
            throw new IllegalArgumentException("Invalid argument");
        cellGrid[row][column] = element;
    }

    @Override
     public CellState get(int row, int column) {
        if(outOfBounds(row, column))
            throw new IllegalArgumentException("Invalid argument");
        return cellGrid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copiedGrid = new CellGrid(rows, cols, null);
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < cols; j++) {
                copiedGrid.set(i, j, get(i,j));
            }
        }
        return copiedGrid;
    }
    
}
